﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2V.Models
{
    public class ChiffreAffaire
    {

            public string num { get; set; }
            public string nom { get; set; }
            public decimal? ChiffreAF { get; set; }

            public decimal? percent
            {
                get
                {
                    return ChiffreAF * 100;
                }
            }
        
    }
    public class Etat_Stock
    {

        public string Client { get; set; }
        public int? QTE { get; set; }
        public System.DateTime? date_vente { get; set; }

    }
    public class facture
    {

        public string numVoiture { get; set; }
        public string marque { get; set; }
        public decimal? pu { get; set; }
        public int? qte { get; set; }
        public decimal? montant { get; set; }

    }
    public class ViewModel
    {

        public client client { get; set; }
        public voiture voiture { get; set; }
        public ventes ventes { get; set; }

    }
}