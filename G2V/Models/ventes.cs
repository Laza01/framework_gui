//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace G2V.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class ventes
    {
        public int Id { get; set; }

        [Required(ErrorMessage = " Le num�ro du client est necessaire.")]
        public string num_client { get; set; }
        [Required(ErrorMessage = " Le num�ro de la voiture est necessaire.")]
        public string num_voiture { get; set; }
        [Required(ErrorMessage = " La Quantit� est necessaire.")]
        public Nullable<int> qte { get; set; }
        [Required(ErrorMessage = " La date de vente est necessaire.")]
        public Nullable<System.DateTime> date_vente { get; set; }


        public virtual client client { get; set; }
        public virtual voiture voiture { get; set; }

    }
}
